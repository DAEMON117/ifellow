class BaseConverter:

    while True:
        try:
            celsius = float(input('Введите цельсии: '))
        except ValueError:
            print('Введено некорректное значение')
            continue
        if type(celsius) == float:
            break

    type_convert = str(input('Выберите тип конвертации (k - кельвины, f - фаренгейты): '))

    def convert(celsius, type_convert):
        if type_convert == 'k':
            return str(celsius + 273.15) + 'K'
        elif type_convert == 'f':
            return str(celsius * (9 / 5) + 32) + '°F'
        else:
            return 'Некорректный тип конвертации'

    print(convert(celsius, type_convert))
