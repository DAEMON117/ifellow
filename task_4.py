hours = 0
minutes = 0

deviation_from_one_hours = 360 / 12
deviation_from_one_minutes = 360 / 60

while True:
    try:
        hours = int(input('Введите часы: '))
    except ValueError:
        print('Введено некорректное значение')
        continue
    if type(hours) == int and (hours >= 0 and hours <= 23):
        break
    else:
        print('Введите часы от 0 до 23')
        continue

while True:
    try:
        minutes = int(input('Введите минуты: '))
    except ValueError:
        print('Введено некорректное значение')
        continue
    if (type(minutes) == int) and (minutes >= 0 and minutes <= 59):
        break
    else:
        print('Введите минуты от 0 до 59')
        continue

def calculation_angle (hours, minutes):
    if hours > 11:
        hours -= 12

    deviation_from_hours = deviation_from_one_hours * hours
    deviation_from_minutes = deviation_from_one_minutes * minutes
    return abs(deviation_from_minutes - deviation_from_hours)

print(str(calculation_angle(hours, minutes)) + '°')