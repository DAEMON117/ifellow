import random

mass = []

for i in range(0, 10):
    mass.append(random.uniform(0, 1))

print(mass)
print()
print('Максимальное значение: ' + str(max(mass)))
print('Минимальное значение: ' + str(min(mass)))
print('Среднее значение: ' + str(sum(mass) / len(mass)))
