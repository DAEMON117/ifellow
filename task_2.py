word = 'Hello'
print(word)

word_unique = "".join(dict.fromkeys(word))
count_chars = []
duplicate_char = []

i = 0
for i in range(len(word_unique)):
    count_chars.append(0)
    j = 0
    for j in range(len(word)):
        if word_unique[i] == word[j]:
            count_chars[i] = count_chars[i] + 1
    print(word_unique[i] + ': ' + str(count_chars[i]))

i = 0
for i in range(len(count_chars)):
    if count_chars[i] > 1:
        duplicate_char.append(word_unique[i])

print('Повторяющиеся символы: ' + str(duplicate_char))
